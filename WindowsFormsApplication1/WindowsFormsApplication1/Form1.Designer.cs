﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.run1 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.filesize = new System.Windows.Forms.Label();
            this.c2 = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.plainkey = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.de = new System.Windows.Forms.TextBox();
            this.rn = new System.Windows.Forms.TextBox();
            this.ew = new System.Windows.Forms.TextBox();
            this.tt = new System.Windows.Forms.TextBox();
            this.ne = new System.Windows.Forms.TextBox();
            this.qw = new System.Windows.Forms.TextBox();
            this.pe = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.deckey = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.biner = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.FileName = new System.Windows.Forms.TextBox();
            this.c1 = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.key = new System.Windows.Forms.RichTextBox();
            this.p1 = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.run2 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.osas = new System.Windows.Forms.RichTextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.RichTextBox();
            this.nd = new System.Windows.Forms.TextBox();
            this.dd = new System.Windows.Forms.TextBox();
            this.cs = new System.Windows.Forms.RichTextBox();
            this.cd = new System.Windows.Forms.RichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label38 = new System.Windows.Forms.Label();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Font = new System.Drawing.Font("Myanmar Text", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(-3, 14);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1312, 572);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1304, 534);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Beranda";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label25.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(617, 450);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 36);
            this.label25.TabIndex = 10;
            this.label25.Text = "Medan";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(555, 185);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(167, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label24.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(628, 486);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 36);
            this.label24.TabIndex = 8;
            this.label24.Text = "2020\r\n";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label23.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(479, 415);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(329, 36);
            this.label23.TabIndex = 7;
            this.label23.Text = "UNIVERSITAS SUMATERA UTARA";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label22.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(367, 379);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(574, 36);
            this.label22.TabIndex = 6;
            this.label22.Text = "FAKULTAS ILMU KOMPUTER DAN TEKNOLOGI INFORMASI ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label21.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(449, 343);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(392, 36);
            this.label21.TabIndex = 5;
            this.label21.Text = "PROGRAM STUDI S-1 ILMU KOMPUTER ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label20.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(580, 145);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(123, 36);
            this.label20.TabIndex = 4;
            this.label20.Text = "161401117\r\n";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label19.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(533, 110);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(219, 36);
            this.label19.TabIndex = 3;
            this.label19.Text = "Ahmad Rais Nasution";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label18.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(603, 74);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 36);
            this.label18.TabIndex = 2;
            this.label18.Text = "Skripsi";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label17.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(512, 38);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(272, 36);
            this.label17.TabIndex = 1;
            this.label17.Text = "ALGORITMA XOR DAN LUC";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label16.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(316, 2);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(633, 36);
            this.label16.TabIndex = 0;
            this.label16.Text = "PENGAMANAN TEKS DENGAN SKEMA HYBRID MENGGUNAKAN ";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.run1);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.filesize);
            this.tabPage2.Controls.Add(this.c2);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.plainkey);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.de);
            this.tabPage2.Controls.Add(this.rn);
            this.tabPage2.Controls.Add(this.ew);
            this.tabPage2.Controls.Add(this.tt);
            this.tabPage2.Controls.Add(this.ne);
            this.tabPage2.Controls.Add(this.qw);
            this.tabPage2.Controls.Add(this.pe);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.deckey);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.biner);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.FileName);
            this.tabPage2.Controls.Add(this.c1);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.key);
            this.tabPage2.Controls.Add(this.p1);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(1304, 534);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Enkripsi";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button7.Location = new System.Drawing.Point(805, 412);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(171, 36);
            this.button7.TabIndex = 40;
            this.button7.Text = "Simpan CipherText";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(532, 334);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(121, 25);
            this.label39.TabIndex = 39;
            this.label39.Text = "Kunci Privat LUC :";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button3.Location = new System.Drawing.Point(996, 413);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 36);
            this.button3.TabIndex = 38;
            this.button3.Text = "Simpan Cipherkey";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_3);
            // 
            // run1
            // 
            this.run1.Location = new System.Drawing.Point(1072, 476);
            this.run1.Margin = new System.Windows.Forms.Padding(4);
            this.run1.Name = "run1";
            this.run1.Size = new System.Drawing.Size(155, 33);
            this.run1.TabIndex = 37;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(949, 480);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(103, 25);
            this.label27.TabIndex = 36;
            this.label27.Text = "Running Time :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(529, 101);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(129, 27);
            this.label26.TabIndex = 35;
            this.label26.Text = "Kunci Publik LUC :";
            // 
            // filesize
            // 
            this.filesize.AutoSize = true;
            this.filesize.Location = new System.Drawing.Point(11, 81);
            this.filesize.Name = "filesize";
            this.filesize.Size = new System.Drawing.Size(0, 25);
            this.filesize.TabIndex = 34;
            // 
            // c2
            // 
            this.c2.Location = new System.Drawing.Point(805, 258);
            this.c2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(475, 125);
            this.c2.TabIndex = 33;
            this.c2.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(704, 315);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 27);
            this.label13.TabIndex = 32;
            this.label13.Text = "Cipherkey  :";
            // 
            // plainkey
            // 
            this.plainkey.Location = new System.Drawing.Point(805, 102);
            this.plainkey.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.plainkey.Name = "plainkey";
            this.plainkey.Size = new System.Drawing.Size(475, 125);
            this.plainkey.TabIndex = 31;
            this.plainkey.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(711, 155);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 27);
            this.label12.TabIndex = 30;
            this.label12.Text = "Plainkey  : ";
            // 
            // de
            // 
            this.de.Location = new System.Drawing.Point(565, 361);
            this.de.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.de.Name = "de";
            this.de.Size = new System.Drawing.Size(100, 33);
            this.de.TabIndex = 29;
            // 
            // rn
            // 
            this.rn.Location = new System.Drawing.Point(565, 288);
            this.rn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rn.Name = "rn";
            this.rn.Size = new System.Drawing.Size(100, 33);
            this.rn.TabIndex = 27;
            // 
            // ew
            // 
            this.ew.Location = new System.Drawing.Point(565, 249);
            this.ew.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ew.Name = "ew";
            this.ew.Size = new System.Drawing.Size(100, 33);
            this.ew.TabIndex = 25;
            // 
            // tt
            // 
            this.tt.Location = new System.Drawing.Point(565, 212);
            this.tt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tt.Name = "tt";
            this.tt.Size = new System.Drawing.Size(100, 33);
            this.tt.TabIndex = 23;
            this.tt.TextChanged += new System.EventHandler(this.tt_TextChanged);
            // 
            // ne
            // 
            this.ne.Location = new System.Drawing.Point(565, 415);
            this.ne.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ne.Name = "ne";
            this.ne.Size = new System.Drawing.Size(100, 33);
            this.ne.TabIndex = 21;
            this.ne.TextChanged += new System.EventHandler(this.ne_TextChanged);
            // 
            // qw
            // 
            this.qw.Location = new System.Drawing.Point(565, 171);
            this.qw.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.qw.Name = "qw";
            this.qw.Size = new System.Drawing.Size(100, 33);
            this.qw.TabIndex = 18;
            // 
            // pe
            // 
            this.pe.Location = new System.Drawing.Point(565, 132);
            this.pe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pe.Name = "pe";
            this.pe.Size = new System.Drawing.Size(100, 33);
            this.pe.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(531, 367);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 27);
            this.label11.TabIndex = 28;
            this.label11.Text = "d :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(525, 292);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 27);
            this.label10.TabIndex = 26;
            this.label10.Text = "Rn :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(531, 252);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 27);
            this.label9.TabIndex = 24;
            this.label9.Text = "e :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(529, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 27);
            this.label8.TabIndex = 22;
            this.label8.Text = "t : ";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(528, 418);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 27);
            this.label7.TabIndex = 20;
            this.label7.Text = "N : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(531, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 27);
            this.label6.TabIndex = 16;
            this.label6.Text = "Q : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(531, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 27);
            this.label5.TabIndex = 15;
            this.label5.Text = "P : ";
            // 
            // deckey
            // 
            this.deckey.Location = new System.Drawing.Point(372, 172);
            this.deckey.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.deckey.Name = "deckey";
            this.deckey.Size = new System.Drawing.Size(153, 41);
            this.deckey.TabIndex = 11;
            this.deckey.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 250);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 27);
            this.label4.TabIndex = 10;
            this.label4.Text = "Plaintext (Binners) :";
            // 
            // biner
            // 
            this.biner.Location = new System.Drawing.Point(16, 277);
            this.biner.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.biner.Name = "biner";
            this.biner.Size = new System.Drawing.Size(112, 105);
            this.biner.TabIndex = 9;
            this.biner.Text = "";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button2.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(73, 448);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(309, 57);
            this.button2.TabIndex = 8;
            this.button2.Text = "Enkripsi";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Location = new System.Drawing.Point(565, 20);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(247, 39);
            this.button1.TabIndex = 7;
            this.button1.Text = "Cari File";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FileName
            // 
            this.FileName.Location = new System.Drawing.Point(8, 23);
            this.FileName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(551, 33);
            this.FileName.TabIndex = 6;
            this.FileName.TextChanged += new System.EventHandler(this.filename_TextChanged);
            // 
            // c1
            // 
            this.c1.Location = new System.Drawing.Point(159, 277);
            this.c1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(323, 105);
            this.c1.TabIndex = 5;
            this.c1.Text = "";
            this.c1.TextChanged += new System.EventHandler(this.c1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(153, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 27);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ciphertext :\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(378, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 27);
            this.label2.TabIndex = 3;
            this.label2.Text = "Kunci XOR : ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Plaintext :";
            // 
            // key
            // 
            this.key.Location = new System.Drawing.Point(373, 126);
            this.key.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.key.Name = "key";
            this.key.Size = new System.Drawing.Size(152, 38);
            this.key.TabIndex = 1;
            this.key.Text = "";
            this.key.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
            // 
            // p1
            // 
            this.p1.Location = new System.Drawing.Point(16, 124);
            this.p1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(334, 105);
            this.p1.TabIndex = 0;
            this.p1.Text = "";
            this.p1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage4.Controls.Add(this.button8);
            this.tabPage4.Controls.Add(this.label40);
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Controls.Add(this.label32);
            this.tabPage4.Controls.Add(this.run2);
            this.tabPage4.Controls.Add(this.label31);
            this.tabPage4.Controls.Add(this.label30);
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.label28);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.osas);
            this.tabPage4.Controls.Add(this.button6);
            this.tabPage4.Controls.Add(this.result);
            this.tabPage4.Controls.Add(this.nd);
            this.tabPage4.Controls.Add(this.dd);
            this.tabPage4.Controls.Add(this.cs);
            this.tabPage4.Controls.Add(this.cd);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Size = new System.Drawing.Size(1304, 534);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Dekripsi";
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button8.Location = new System.Drawing.Point(181, 192);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(137, 47);
            this.button8.TabIndex = 39;
            this.button8.Text = "Cari CipherText";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(28, 242);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(121, 25);
            this.label40.TabIndex = 38;
            this.label40.Text = "Kunci Privat LUC :";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button5.Location = new System.Drawing.Point(33, 192);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(137, 47);
            this.button5.TabIndex = 37;
            this.button5.Text = "Cari CipherKey";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(321, 311);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(82, 25);
            this.label32.TabIndex = 36;
            this.label32.Text = "Ciphertext :";
            // 
            // run2
            // 
            this.run2.Location = new System.Drawing.Point(1140, 462);
            this.run2.Margin = new System.Windows.Forms.Padding(4);
            this.run2.Name = "run2";
            this.run2.Size = new System.Drawing.Size(145, 33);
            this.run2.TabIndex = 34;
            this.run2.TextChanged += new System.EventHandler(this.run2_TextChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(999, 470);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(103, 25);
            this.label31.TabIndex = 33;
            this.label31.Text = "Running Time :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(28, 311);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 25);
            this.label30.TabIndex = 32;
            this.label30.Text = "Kunci XOR :\r\n";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(176, 272);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 25);
            this.label29.TabIndex = 31;
            this.label29.Text = "N :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(28, 268);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(27, 25);
            this.label28.TabIndex = 30;
            this.label28.Text = "d :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(692, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 25);
            this.label15.TabIndex = 29;
            this.label15.Text = "Plaintext : ";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // osas
            // 
            this.osas.Location = new System.Drawing.Point(697, 34);
            this.osas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.osas.Name = "osas";
            this.osas.Size = new System.Drawing.Size(588, 313);
            this.osas.TabIndex = 28;
            this.osas.Text = "";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button6.Location = new System.Drawing.Point(697, 397);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(405, 48);
            this.button6.TabIndex = 27;
            this.button6.Text = "Dekripsi";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // result
            // 
            this.result.Location = new System.Drawing.Point(327, 340);
            this.result.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(268, 107);
            this.result.TabIndex = 26;
            this.result.Text = "";
            this.result.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // nd
            // 
            this.nd.Location = new System.Drawing.Point(213, 266);
            this.nd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nd.Name = "nd";
            this.nd.Size = new System.Drawing.Size(100, 33);
            this.nd.TabIndex = 25;
            this.nd.TextChanged += new System.EventHandler(this.nd_TextChanged);
            // 
            // dd
            // 
            this.dd.Location = new System.Drawing.Point(56, 266);
            this.dd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dd.Name = "dd";
            this.dd.Size = new System.Drawing.Size(100, 33);
            this.dd.TabIndex = 24;
            this.dd.TextChanged += new System.EventHandler(this.dd_TextChanged);
            // 
            // cs
            // 
            this.cs.Location = new System.Drawing.Point(33, 338);
            this.cs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cs.Name = "cs";
            this.cs.Size = new System.Drawing.Size(253, 107);
            this.cs.TabIndex = 23;
            this.cs.Text = "";
            // 
            // cd
            // 
            this.cd.Location = new System.Drawing.Point(33, 60);
            this.cd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cd.Name = "cd";
            this.cd.Size = new System.Drawing.Size(561, 125);
            this.cd.TabIndex = 21;
            this.cd.Text = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(28, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 25);
            this.label14.TabIndex = 20;
            this.label14.Text = "Cipherkey :";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage3.Controls.Add(this.label38);
            this.tabPage3.Controls.Add(this.richTextBox4);
            this.tabPage3.Controls.Add(this.label37);
            this.tabPage3.Controls.Add(this.richTextBox3);
            this.tabPage3.Controls.Add(this.label36);
            this.tabPage3.Controls.Add(this.richTextBox2);
            this.tabPage3.Controls.Add(this.label35);
            this.tabPage3.Controls.Add(this.richTextBox1);
            this.tabPage3.Controls.Add(this.label34);
            this.tabPage3.Controls.Add(this.label33);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Size = new System.Drawing.Size(1304, 534);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tentang";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(533, 42);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(233, 30);
            this.label38.TabIndex = 9;
            this.label38.Text = "ALGORITMA XOR DAN LUC";
            // 
            // richTextBox4
            // 
            this.richTextBox4.Location = new System.Drawing.Point(784, 341);
            this.richTextBox4.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(475, 150);
            this.richTextBox4.TabIndex = 8;
            this.richTextBox4.Text = resources.GetString("richTextBox4.Text");
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(779, 309);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(167, 30);
            this.label37.TabIndex = 7;
            this.label37.Text = "Hybrid Criptosystem";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(784, 140);
            this.richTextBox3.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(475, 143);
            this.richTextBox3.TabIndex = 6;
            this.richTextBox3.Text = resources.GetString("richTextBox3.Text");
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(793, 108);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(115, 30);
            this.label36.TabIndex = 5;
            this.label36.Text = "Algoritm LUC";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(19, 341);
            this.richTextBox2.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(475, 150);
            this.richTextBox2.TabIndex = 4;
            this.richTextBox2.Text = resources.GetString("richTextBox2.Text");
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(21, 313);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(126, 30);
            this.label35.TabIndex = 3;
            this.label35.Text = "Algoritma XOR";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(19, 137);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(475, 143);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(399, 12);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(533, 30);
            this.label34.TabIndex = 1;
            this.label34.Text = "PENGAMANAN TEKS DENGAN SKEMA HYBRID MENGGUNAKAN ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(21, 108);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(98, 30);
            this.label33.TabIndex = 0;
            this.label33.Text = "Kriptografi ";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage5.Location = new System.Drawing.Point(4, 34);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage5.Size = new System.Drawing.Size(1304, 534);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Bantuan";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 582);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox dd;
        private System.Windows.Forms.RichTextBox cs;
        private System.Windows.Forms.RichTextBox cd;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox nd;
        private System.Windows.Forms.RichTextBox deckey;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox biner;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox c1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox key;
        private System.Windows.Forms.RichTextBox p1;
        private System.Windows.Forms.TextBox de;
        private System.Windows.Forms.TextBox rn;
        private System.Windows.Forms.TextBox ew;
        private System.Windows.Forms.TextBox tt;
        private System.Windows.Forms.TextBox ne;
        private System.Windows.Forms.TextBox qw;
        private System.Windows.Forms.TextBox pe;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox c2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RichTextBox plainkey;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RichTextBox result;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RichTextBox osas;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label filesize;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox run1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox run2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox FileName;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
    }
}

