﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Security.Cryptography;
using Microsoft.Office.Interop.Word;
namespace WindowsFormsApplication1
{
    
    public partial class Form1 : Form
    {
        char[] asciinum = { '←','↑','→','↓','↔','↵','⇐','⇑','⇒','⇓','\n','\t','∂','∃','∅','∇',
            '∈','∉','∋','∏','∑','√','∝','∞','∠','∧','∨','∩','∪','∫','∴','∼',
            ' ','!','\"','#','$','%','&','\'','(',')','*','+',',','-','.','/',
            '0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?',
            '@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
            'P','Q','R','S','T','U','V','W','X','Y','Z','[','\\',']','^','_',
            '`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
            'p','q','r','s','t','u','v','w','x','y','z','{','|','}','~','≈',
            '≠','≡','≤','≥','⊂','⊃','⊄','⊆','⊇','⊕','⊗','α','β','γ','δ','ε',
            'ζ','η','θ','ι','κ','λ','μ','ν','ξ','ο','π','ρ','σ','ς','τ','υ',
            '¶','¡','¢','£','¤','¥','¦','§','¨','©','ª','«','¬','Ⅲ','®','¯',
            '°','±','²','³','´','µ','Ⅱ','•','¸','¹','º','»','¼','½','¾','¿',
            'À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï',
            'Ð','Ñ','Ò','Ó','Ô','Õ','Ö','×','Ø','Ù','Ú','Û','Ü','Ý','Þ','ß',
            'à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï',
            'ð','ñ','ò','ó','ô','õ','ö','÷','ø','ù','ú','û','ü','ý','þ','ÿ'
         };
        public StringBuilder binner = new StringBuilder();
        public StringBuilder dec = new StringBuilder();
        public BigInteger p = 0, q = 0,en = 0,te=0, eh=0, d=0;
        public Random rnd = new Random();
        public BigInteger[] v = new BigInteger[0];
        public string bin = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var watch = new System.Diagnostics.Stopwatch(); //ini untuk inisialisasi kan runtime
            watch.Start(); //ini untuk memulai menghitung waktu
            bin = p1.Text;
            binner.Clear();
            c1.Text = "";
            int keys = 0;
            deckey.Text = "";
            key.Text = "";
            string memkey = @"c:\temp\key.temp";
            string memdeckey = @"c:\temp\deckey.temp";
            string mc1 = @"c:\temp\c1.temp";
            string mc2 = @"c:\temp\c2.temp";
            if (File.Exists(memkey))
            {
                File.Delete(memkey);
            }
            if (File.Exists(memdeckey))
            {
                File.Delete(memdeckey);
            }
            if (File.Exists(mc1))
            {
                File.Delete(mc1);
            }
            if (File.Exists(mc2))
            {
                File.Delete(mc2);
            }
            using (StreamWriter sh2 = new StreamWriter(mc1))
            {
                using (StreamWriter sh1 = new StreamWriter(memdeckey))
                {
                    using (StreamWriter sh = new StreamWriter(memkey))
                    {
                        for (int i = 0; i < bin.Length; i++)
                        {
                            keys = rnd.Next(0, 255);
                            //deckey.Text = deckey.Text + asciinum[keys];
                            sh1.Write(asciinum[keys]);
                            sh.Write(Convert.ToString(keys, 2).PadLeft(8, '0') + "");
                            //key.Text = key.Text + Convert.ToString(keys, 2).PadLeft(8, '0') + "";
                            for (int j = 0; j < 256; j++)
                            {
                                char temp = bin[i];
                                if (temp.Equals(asciinum[j]) == true)
                                {
                                    binner.Append(Convert.ToString(j, 2).PadLeft(8, '0') + "");
                                    sh2.Write(asciinum[(keys ^ j)]);
                                    //c1.Text = c1.Text + asciinum[(keys ^ j)];
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            
            using (StreamReader sr = new StreamReader(memdeckey))
            {
                deckey.Text = sr.ReadToEnd();
            }
            using (StreamReader sr = new StreamReader(mc1))
            {
                c1.Text = sr.ReadToEnd();
            }
            using (StreamReader sr = new StreamReader(memkey))
            {
                key.Text = sr.ReadToEnd();
            }
            biner.Text = binner.ToString();
            plainkey.Text = deckey.Text;
            // result.Text = c1.Text;

            /*
          BigInteger p = agrawalor(rnd.Next(50, 100));
          BigInteger q = agrawalor(rnd.Next(50, 100));

          /*
          fermats way
          */
            BigInteger p = 0;
          BigInteger q = 0;

          p = rnd.Next(5, 100);
          while (prime(p) != 1)
          {
              p = rnd.Next(5, 100);
          }
          q = rnd.Next(5, 100);
          while ((prime(q) != 1) && (p != q))
          {
              q = rnd.Next(5, 100);
          }

          
            pe.Text = p.ToString();
            qw.Text = q.ToString();
            en = BigInteger.Multiply(p, q);
            ne.Text = en.ToString();
            BigInteger temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0;
            temp1 = BigInteger.Subtract(p, 1);
            temp2 = BigInteger.Subtract(q, 1);
            temp3 = BigInteger.Add(p, 1);
            temp4 = BigInteger.Add(q, 1);
            te = BigInteger.Multiply(temp4, BigInteger.Multiply(temp2, BigInteger.Multiply(temp1, temp3)));
            tt.Text = te.ToString();
            int gecede = 0, gecedew = 0;
            BigInteger hasillcm = 0;
            while (gecede != 1 || gecedew != 1)
            {
                eh = rnd.Next(2, 100);
                gecede = (int)GCD(eh, te);
                ew.Text = eh.ToString();
                hasillcm = LCM(temp1, temp2);
                hasillcm = LCM(hasillcm, temp3);
                hasillcm = LCM(hasillcm, temp4);

                rn.Text = hasillcm.ToString();
                gecedew = (int)GCD(eh, hasillcm);
            }

            d = diophantin(eh, hasillcm);
            //Console.WriteLine(eh + " + " + hasillcm);
            de.Text = d.ToString();
            nd.Text = ne.Text;
            dd.Text = de.Text;
            c2.Text = "";
            StringBuilder plainteks = new StringBuilder();
            plainteks.Clear();
            plainteks.Append(plainkey.Text);
            int enn = int.Parse(ne.Text);
            v = new BigInteger[int.Parse(ew.Text) + 1];
            using (StreamWriter sh2 = new StreamWriter(mc2))
            {
                for (int i = 0; i < plainteks.Length; i++)
                {
                    for (int j = 0; j < 256; j++)
                    {
                        if (plainteks[i].Equals(asciinum[j]) == true)
                        {
                            int keyindex = j;
                            v[0] = 2;
                            v[1] = keyindex;
                            int iw = 0;
                            for (iw = 2; iw <= int.Parse(ew.Text); iw++)
                            {
                                BigInteger wew = ((keyindex * v[iw - 1]) - (v[iw - 2]));
                                if (wew < 0)
                                {
                                    v[iw] = enn - (wew * (-1));
                                }
                                else
                                {
                                    v[iw] = BigInteger.Remainder((wew), enn);
                                }
                                //Console.WriteLine(iw + " . " + v[iw] + " = " + keyindex + " x "+ v[iw - 1]+ ")-(" + v[iw - 2] +")) % " + enn);
                            }
                            sh2.Write(v[int.Parse(ew.Text)] + " ");
                            //c2.Text += v[int.Parse(ew.Text )] + " ";
                            break;
                        }
                    }
                }
            }
            using (StreamReader sr = new StreamReader(mc2))
            {
              c2.Text = sr.ReadToEnd();
            }
            //cd.Text = c2.Text;
            //dd.Text = d.ToString();
            watch.Stop(); // menghentikan runtime;
            run1.Text = Math.Round(Convert.ToDecimal(watch.ElapsedMilliseconds) / 1000, 4).ToString() + " ms";

        }


       

        private void button3_Click(object sender, EventArgs e)
        {
            
            p =  rnd.Next(2, 50);
            while(prime(p) != 1)
            {
                p = rnd.Next(2, 50);
            }
            q = rnd.Next(2, 50);
            while ((prime(q) != 1) && (p != q))
            {
                q = rnd.Next(2, 50);
            }

            pe.Text = p.ToString();
            qw.Text = q.ToString();
            en = BigInteger.Multiply(p, q);
            ne.Text = en.ToString();
            BigInteger temp1 = 0, temp2 = 0 , temp3 = 0 , temp4 = 0;
            temp1 = BigInteger.Subtract(p, 1);
            temp2 = BigInteger.Subtract(q, 1);
            temp3 = BigInteger.Add(p, 1);
            temp4 = BigInteger.Add(q, 1);
            te = BigInteger.Multiply(temp1,BigInteger.Multiply(temp2,BigInteger.Multiply(temp3,temp4)));
            tt.Text = te.ToString();
            int gecede = 0 , gecedew = 0;
            BigInteger hasillcm = 0;
            while (gecede != 1 || gecedew != 1)
            {               
            eh = rnd.Next(2, 100);
            gecede = (int)GCD(eh, te);
            ew.Text = eh.ToString();
            hasillcm = LCM(temp1, temp2);
            hasillcm = LCM(hasillcm, temp3);
            hasillcm = LCM(hasillcm, temp4);
            
            rn.Text = hasillcm.ToString();
                gecedew = (int)GCD(eh, hasillcm);
            }

            d = diophantin(eh, hasillcm);
            //Console.WriteLine(eh + " + " + hasillcm);
            dd.Text = d.ToString();
            nd.Text = ne.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void c2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
           
        }

        private void button3_Click_1(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var watch = new System.Diagnostics.Stopwatch(); //ini untuk inisialisasi kan runtime
            watch.Start(); //ini untuk memulai menghitung waktu
            StringBuilder haha = new StringBuilder();
            haha.Clear();
            haha.Append(cd.Text);
            cs.Text = "";
            string mc2 = @"c:\temp\kunciXOR.temp";
            if (File.Exists(mc2))
            {
                File.Delete(mc2);
            }
            string mc = @"c:\temp\dekripsi_hasil.temp";
            if (File.Exists(mc))
            {
                File.Delete(mc);
            }
            BigInteger temps = 0;
            BigInteger enns = BigInteger.Parse(nd.Text);
            BigInteger ddec = BigInteger.Parse(dd.Text);
            try {
                v = new BigInteger[(int)ddec + 1];
            }
            catch
            {
                MessageBox.Show(ddec+1+"");
            }
            string tem = "";
            try
            {
                for (int i = 0; i < haha.Length; i++)
            {

                if (haha[i].Equals(' ') == true)
                {

                    temps = BigInteger.Parse(tem);
                    tem = "";
                    v[0] = 2;
                    v[1] = temps;

                    //Console.WriteLine(v[0] + "\n" + v[1]);
                    for (int j = 2; j <= ddec; j++)
                    {
                        BigInteger wew = ((temps * v[j - 1]) - (v[j - 2]));
                        if (wew < 0)
                        {
                            v[j] = enns - (wew * (-1));
                        }
                        else
                        {
                            v[j] = BigInteger.Remainder((wew), enns);
                        }
                        // Console.WriteLine(j+". "+v[j]+" = ((" +temps+" * "+v[j - 1]+") - ("+v[j - 2]+")) mod "+enns);
                    }
                        cs.Text = cs.Text + asciinum[(int)v[(int)ddec]];

                        using (StreamWriter sh2 = new StreamWriter(mc2))
                        {
                           

                            sh2.Write(cs.Text);
                        }
                   
                }
                else
                    tem = tem + haha[i];
            }
            }
                    catch
            {
                MessageBox.Show("there is no prime betwen p or q");
            }
            StringBuilder csatu = new StringBuilder();
            StringBuilder csdua = new StringBuilder();
            StringBuilder fresult= new StringBuilder();
            csatu.Clear();
            osas.Text = "";
            csdua.Clear();
            csatu.Append(cs.Text);
            csdua.Append(result.Text);
            for(int i = 0; i< csatu.Length; i++)
            {
                int temp1 = 0, temp2=0;
                for (int j = 0; j<256; j++)
                {
                    if (csatu[i].Equals(asciinum[j]) == true)
                    {
                        temp1 = j;
                        break;
                    }
                }
                for (int j = 0; j < 256; j++)
                {
                    if (csdua[i].Equals(asciinum[j]) == true)
                    {
                        temp2 = j;
                        break;
                    }
                }
                fresult.Append(asciinum[(temp1 ^ temp2)]);
                using (StreamWriter sh = new StreamWriter(mc))
                {
                    sh.Write(fresult);
                }
                osas.Text =  osas.Text + asciinum[(temp1 ^ temp2)];
                
            }
            watch.Stop(); // menghentikan runtime;
            run2.Text = Math.Round(Convert.ToDecimal(watch.ElapsedMilliseconds) / 1000, 4).ToString() + " ms";
          
        }

        public String convbinner(BigInteger n)
        {
            BigInteger x = n, pemicu = 1;
            String hasil = "";
            while (x > 0)
            {
                hasil = "" + x % 2 + "" + hasil;
                x = x / 2;
            }
            return hasil;
        }
        public  BigInteger sam(BigInteger x, String bin, BigInteger y)
        {
            int i;
            BigInteger z = 1;
            for (i = 0; i < bin.Length; i++)
            {
                if (bin[i] == '0')
                {
                    z = (BigInteger.Multiply(z, z)) % y;
                    //Console.WriteLine(i + ". z =(" + BigInteger.Multiply(z, z) + ") mod " + y + " = " + z);
                }
                if (bin[i] == '1')
                {
                    z = (BigInteger.Multiply((BigInteger.Multiply(z, z)), x)) % y;
                    //Console.WriteLine(i + ". z =((" + BigInteger.Multiply(z, z) + ")*" + x + ") mod " + y + "= " + z);
                }
            }
            return z;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = @"D:\program skirps\FILE UJI";
            openFileDialog1.Filter = "";
            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                try
                {
                    long size = 1;
                    string text = File.ReadAllText(file);
                    size = new FileInfo(openFileDialog1.FileName).Length;
                    FileInfo fi = new FileInfo(file);
                    string extn = fi.Extension;
                    Console.WriteLine(extn.Equals(".docx"));
                    if (extn.Equals(".docx"))
                    {
                        StringBuilder textss = new StringBuilder();
                        Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
                        object miss = System.Reflection.Missing.Value;
                        object path = file;
                        object readOnly = true;
                        Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref path, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);

                        for (int i = 0; i < docs.Paragraphs.Count; i++)
                        {
                            textss.Append(docs.Paragraphs[i + 1].Range.Text.ToString());
                        }
                        FileName.Text = file;
                        Console.WriteLine(file);
                        p1.Text = textss.ToString();
                    }
                    else
                    if (extn.Equals(".pdf"))
                    {
                        StringBuilder textss = new StringBuilder();
                        using (PdfReader reader = new PdfReader(file))
                        {
                            for (int i = 1; i <= reader.NumberOfPages; i++)
                            {
                                textss.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                            }
                        }

                        p1.Text = textss.ToString();
                    }
                    else
                    {
                        FileName.Text = file;
                        Console.WriteLine(file);
                        p1.Text = text.ToString();
                    }


                }
                catch (IOException)
                {
                }
                Console.WriteLine("upload file berhasil");
            }
            else
            {
                Console.WriteLine("upload file gagal");
                long filesizer = new System.IO.FileInfo(FileName.Text).Length;
                filesize.Text = filesizer + " bytes";
            }
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void result_TextChanged(object sender, EventArgs e)
        {

        }

        private void c1_TextChanged(object sender, EventArgs e)
        {

        }

        private void filename_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void tt_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_2(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "All files (*.*)|*.*"; 
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                
            {
                string textVal = osas.Text;
                File.WriteAllText(saveFileDialog1.FileName, textVal);
            }
        }

    private void button3_Click_2(object sender, EventArgs e)
        {

        }

        private void button3_Click_3(object sender, EventArgs e)
        {
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "All files (*.*)|*.*";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string txtVal = c2.Text;
                    File.WriteAllText(saveFileDialog1.FileName, txtVal);
                }

                MessageBox.Show("Cipherkey Berhasil Disimpan", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = @"D:\program skirps\key";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string filetext = File.ReadAllText(openFileDialog1.FileName);
                cd.Text = filetext;

            }
        }

        private void nd_TextChanged(object sender, EventArgs e)
        {

        }

        private void dd_TextChanged(object sender, EventArgs e)
        {

        }

        private void ne_TextChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
           
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"D:\program skirps\key";
            saveFileDialog1.Filter = "All files (*.*)|*.*";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                string txtVal2 = c1.Text;
                File.WriteAllText(saveFileDialog1.FileName, txtVal2);
            }

            MessageBox.Show("Cipherkey Berhasil Disimpan", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = @"D:\program skirps\key";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string filetext = File.ReadAllText(openFileDialog1.FileName);
                result.Text = filetext;

            }
        }

        private void run2_TextChanged(object sender, EventArgs e)
        {

        }

        public BigInteger agrawalor(BigInteger n)
        {
            int z = 0;
            //z = rnd.Next(3, 999999999 - 2);
            z = rnd.Next(3, (int)n - 2);
            if ((sam(1 + z, (convbinner(n)), n)) == (1 + (sam(z, convbinner(n), n)) % n) && (n % 2 != 0 && n % 5 != 0))
                return n;
            else
            {
                /*
                var rng = new RNGCryptoServiceProvider();
                byte[] bytes = new byte[512 / 8];
                rng.GetBytes(bytes);
                BigInteger pew = new BigInteger(bytes);
                return agrawalor(pew);
                */
                return agrawalor(rnd.Next(50, 100));
            }


        }
        public BigInteger prime(BigInteger x) //fermats
        {
            BigInteger i = 2;
            BigInteger hasil = 0;
            try {
                while (i != x)
                {
                    hasil = (BigInteger.Pow(i, (int)x - 1)) % x;
                    if (hasil != 1)
                    {
                        return 0;
                        break;
                    }
                    i++;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(hasil + " : " + "error");
            }
            return 1;
        }
        public BigInteger GCD(BigInteger x, BigInteger y)
        {

            BigInteger c = x % y;
            while (c != 0)
            {
                x = y;
                y = c;
                c = x % y;

            }
            return y;
        }
        public BigInteger LCM(BigInteger x, BigInteger y)
        {
            BigInteger result = 0;
            result = (BigInteger.Multiply(x, y)) / (GCD (x,y));
            return result;
        }
        public  BigInteger diophantin(BigInteger mi, BigInteger ni)
        {
            BigInteger x = 0, y = 1, d0 = 0, k = 0, ew = 0, uw = 0;
            BigInteger x2 = 1, y2 = 0, d2 = 0;
            BigInteger x3 = 0, y3 = 0, d3 = 0;
            if (mi > ni)
            {
                ew = mi;
                uw = ni;
                d0 = mi;
                d2 = ni;
            }
            else
            {
                ew = ni;
                uw = mi;
                d0 = ni;
                d2 = mi;
            }
            //Console.WriteLine("ew = " + d0 + " uw = " + d2 + " x = " + x + " y = " + y + " d0 = " + d0 + " k =" + k);
            k = BigInteger.Divide(d0, d2);
            x3 = BigInteger.Subtract(x, (BigInteger.Multiply(x2, k)));
            y3 = BigInteger.Subtract(y, (BigInteger.Multiply(y2, k)));
            d3 = BigInteger.Subtract(d0, (BigInteger.Multiply(d2, k)));

            //Console.WriteLine("ew = " + d0 + " uw = " + d2 + " x2 = " + x2 + " y2 = " + y2 + " d2 = " + d2 + " k =" + k);

            try
            {
                k = BigInteger.Divide(d2, d3);
            }
            catch (Exception ex)
            {
                // handle exception here
                Console.WriteLine(d0 + " / " + d2);
            }
            //Console.WriteLine("ew = " + d0 + " uw = " + d2 + " x3 = " + x3 + " y3 = " + y3 + " d3 = " + d3 + " k =" + k);
            try
            {
                do
                {
                    x = x2;
                    y = y2;
                    d0 = d2;
                    x2 = x3;
                    y2 = y3;
                    d2 = d3;
                    k = BigInteger.Divide(d0, d2);
                    x3 = BigInteger.Subtract(x, (BigInteger.Multiply(x2, k)));
                    y3 = BigInteger.Subtract(y, (BigInteger.Multiply(y2, k)));
                    d3 = BigInteger.Subtract(d0, (BigInteger.Multiply(d2, k)));


                    //Console.WriteLine("ew = " + d0 + " uw = " + d2 + " x3 = " + x3 + " y3 = " + y3 + " d3 = " + d3 + " k =" + k);
                }
                while (d3 != 1);
            }
            catch
            {
                MessageBox.Show(d0 + " " + d2);
            }
            if (x3 < 0)
            {
                return ew - (x3 * -1);
            }
            else
                return x3;
        }
    }
}
